package com.example.json;

import com.example.pojo.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaToJson {
	
	public static void main (String[] st) {
		Employee employee = new Employee(1, "Amol Hajare", "pune"); 
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			String jsonStr = objectMapper.writeValueAsString(employee);
			System.out.println(jsonStr);		
		
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

}
